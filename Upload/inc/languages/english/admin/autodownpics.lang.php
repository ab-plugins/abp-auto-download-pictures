<?php
/**
 * Auto download pictures plugin :
 * When an user post an external picture,
 * the picture is downloaded and the local one is
 * displayed.
 */
 
$l['autodownpics_name'] = "ABP Auto-download Pictures";
$l['autodownpics_desc'] = "When an user post an external picture, the picture is downloaded and the local one is displayed.";
$l['autodownpics_setting_title'] = "Settings for Auto-download Pictures";
$l['autodownpics_setting_description'] = "Choose path and allowed groups";

$l['autodownpics_path'] = "Download path";
$l['autodownpics_path_desc'] = "The path must be accessible by an url. Default: uploads/userpics/<br /><b>The plugin WILL NOT create the directory.</b>";
$l['autodownpics_groups'] = "Groups";
$l['autodownpics_groups_desc'] = "When a member of one of the selected groups use an external image, it is automaticaly downloaded.<br />Choose carefully.";
$l['autodownpics_forums'] = "Forums";
$l['autodownpics_forums_desc'] = "Choose in which forums the plugin will be working.";

$l['autodownpics_working'] = "(Unused yet) Working mode";
$l['autodownpics_working_desc'] = "You can choose to have the downloaded picture showed as a standard one or as an attachment<br /><i>Future option</i>";
$l['autodownpic_wimage'] = "Show as image";
$l['autodownpic_wattachment'] = "Show as attachment";
