<?php
/**
 * Auto download pictures plugin :
 * When an user post an external picture,
 * the picture is downloaded and the local one is
 * displayed.
 * �2015 CrazyCat
 */

if (!defined("IN_MYBB"))
    die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");


//$plugins->add_hook("datahandler_post_insert_post", "autodownpics_run");
//$plugins->add_hook("datahandler_post_insert_thread_post", "autodownpics_run");
$plugins->add_hook("datahandler_post_update", "autodownpics_run");
$plugins->add_hook("datahandler_post_validate_post", "autodownpics_run");
$plugins->add_hook("admin_config_plugins_activate_commit", "autodownpics_gosettings");


/**
 * Displayed informations
 */
function autodownpics_info()
{
    global $lang;
    $lang->load("autodownpics");
    return array(
        "name" => $lang->autodownpics_name,
        "description" => $lang->autodownpics_desc,
        "author" => "CrazyCat",
        "version" => "0.3",
        "compatibility" => "18*"
    );
}

/**
 * Install procedure
 * Just add the setting to MyBB
 */
function autodownpics_install()
{
    global $db, $lang;
    $lang->load("autodownpics");

    $settinggroups = array(
        "name" => "autodownpics",
        "title" => $lang->autodownpics_setting_title,
        "description" => $lang->autodownpics_setting_description,
        "disporder" => "0",
        "isdefault" => 0
    );

    $db->insert_query("settinggroups", $settinggroups);
    $gid = $db->insert_id();

    $settings[] = array(
        "name" => "autodownpics_path",
        "title" => $lang->autodownpics_path,
        "description" => $lang->autodownpics_path_desc,
        "optionscode" => "text",
        "value" => "uploads/userpics/",
        "disporder" => 1
    );

    $settings[] = array(
        "name" => "autodownpics_groups",
        "title" => $lang->autodownpics_groups,
        "description" => $lang->autodownpics_groups_desc,
        "optionscode" => "groupselect",
        "value" => "",
        "disporder" => 2
    );

    $settings[] = array(
        "name" => "autodownpics_forums",
        "title" => $lang->autodownpics_forums,
        "description" => $lang->autodownpics_forums_desc,
        "optionscode" => "forumselect",
        "value" => "",
        "disporder" => 3
    );

    $settings[] = array(
        "name" => "autodownpics_working",
        "title" => $lang->autodownpics_working,
        "description" => $lang->autodownpics_working_desc,
        "optionscode" => "radio\np=" . $lang->autodownpic_wimage . "\na=" . $lang->autodownpic_wattachment,
        "value" => "p",
        "disporder" => 4
    );

    foreach ($settings as $setting) {
        $insert = array(
            "name" => $db->escape_string($setting['name']),
            "title" => $db->escape_string($setting['title']),
            "description" => $db->escape_string($setting['description']),
            "optionscode" => $db->escape_string($setting['optionscode']),
            "value" => $db->escape_string($setting['value']),
            "disporder" => $setting['disporder'],
            "gid" => $gid,
        );
        $db->insert_query("settings", $insert);
    }

    // Create table mybb_autodownpics
    $collation = $db->build_create_table_collation();

    if (!$db->table_exists("autodownpics")) {
        // Main table, description of contest
        $table = "CREATE TABLE " . TABLE_PREFIX . "autodownpics (
            adpid int(11) NOT NULL AUTO_INCREMENT,
            adpsourcedesc text NOT NULL,
            adptarget varchar(256) NOT NULL,
            adptime datetime NOT NULL,
            PRIMARY KEY (adpid)
            ) ENGINE = MYISAM{$collation};
        ";
        $db->write_query($table);
    }
    rebuild_settings();
}

/**
 * Uninstall function
 * Remove settings and templates
 * @see imgur_deactivate
 */
function autodownpics_uninstall()
{
    global $db;
    $db->delete_query("settings", "name LIKE 'autodownpics_%'");
    $db->delete_query("settinggroups", "name = 'autodownpics'");
    if ($db->table_exists("autodownpics")) {
        $db->drop_table("autodownpics");
    }
    rebuild_settings();
}

/**
 * Checks if the plugin is installed or not
 */
function autodownpics_is_installed()
{
    global $db;
    if ($db->table_exists("autodownpics")) {
        return true;
    }
    return false;
}

/**
 * Plugin activation
 * Do nothing
 */
function autodownpics_activate()
{
    rebuild_settings();
}

function autodownpics_gosettings()
{
    global $db, $mybb;
	if ($mybb->input['plugin'] == 'autodownpics') {
		$query = $db->simple_select('settinggroups', 'gid', "name='autodownpics'");
		$row = $db->fetch_array($query);
		flash_message("You <b>must</b> configure this plugin", "error");
		admin_redirect("index.php?module=config-settings&action=change&gid=" . $row['gid']);
	}
}

/**
 * Plugin deactivation
 * Do nothing
 */
function autodownpics_deactivate()
{
    rebuild_settings();
}

/**
 * main function
 * @var $post
 */
function autodownpics_run(&$post)
{
    global $mybb;

    // First check: no forum where the plugin is applyed, or no group allowed
    if (empty($mybb->settings['autodownpics_forums']) || empty($mybb->settings['autodownpics_groups'])) {
        return;
    }

    // Checking for groups
    if ($mybb->settings['autodownpics_groups'] != -1) {
        $gids = explode(',', $mybb->settings['autodownpics_groups']);
        $ugid[] = $mybb->user['usergroup'];
        $ugid = array_merge($ugid, explode(',', $mybb->user['additionalgroups']));
        if (count(array_intersect($gids, $ugid)) == 0 && count(array_intersect($ugid, $gids)) == 0) {
            return;
        }
    }


    // Checking for forum
    if ($mybb->settings['autodownpics_forums'] != -1) {
        $cfid = $post->data['fid'];
        $fids = explode(',', $mybb->settings['autodownpics_forums']);
        if (!in_array($cfid, $fids)) {
            // Not a forum in which the plugin is active
            return;
        }
    }

    $pattern = "#\[img([^\]]*\]\r?\n?)(https?:\/\/([^<>\"']+?))\[\/img\]#is";
    if (!empty($post->post_insert_data['message'])) {
        $post->post_insert_data['message'] = preg_replace_callback($pattern, 'autodownpics_callback', $post->post_insert_data['message']);
    } elseif (!empty($post->post_update_data['message'])) {
        $post->post_update_data['message'] = preg_replace_callback($pattern, 'autodownpics_callback', $post->post_update_data['message']);
    } elseif (!empty($post->thread_insert_data['message'])) {
        $post->thread_insert_data['message'] = preg_replace_callback($pattern, 'autodownpics_callback', $post->thread_insert_data['message']);
    } elseif (!empty($post->thread_update_data['message'])) {
        $post->thread_update_data['message'] = preg_replace_callback($pattern, 'autodownpics_callback', $post->thread_update_data['message']);
    } elseif (!empty($post->data['message'])) {
        $post->data['message'] = preg_replace_callback($pattern, 'autodownpics_callback', $post->data['message']);
    }

}

/**
 * Save (if needed) the image
 * and returns the new url
 */
function autodownpics_callback($matches)
{
    global $mybb, $db;
    $content = $matches[0];
    if (strpos($content, $mybb->settings['bburl']) !== false) {
        return $content;
    }
    // Let's check if the picture is not already in our database
    $query = $db->simple_select("autodownpics", "*", "adpsourcedesc='" . $db->escape_string($matches[2]) . "'");
    if ($db->num_rows($query) > 0) {
        // The picture already exists in database, just replacing the img url
        $newpic = $db->fetch_field($query, 'adptarget');
        $content = str_replace($matches[2], $mybb->settings['bburl'] . '/' . $mybb->settings['autodownpics_path'] . $newpic, $content);
        return $content;
    }

    $copy = adp_copyimage($matches[2]);
    if ($copy['errCode'] == 0) {
        $content = str_replace($matches[2], $mybb->settings['bburl'] . '/' . $mybb->settings['autodownpics_path'] . $copy['local'], $content);
        $insert = array(
            'adpsourcedesc' => $db->escape_string($matches[2]),
            'adptarget' => $copy['local'],
            'adptime' => date('Y-m-d H:i:s')
        );
        $db->insert_query('autodownpics', $insert);
    } else {
        $content .= "\n".$copy['errMessage'];
    }
    return $content;
}

/**
 * Create local image
 * @var string $imgurl Source url
 * @return array (error code, error message, local file)
 */
function adp_copyimage($imgurl)
{
    global $mybb;
    $filename = basename($imgurl);
    $ext = strtolower(substr($filename, strrpos($filename, ".")));
    // Not proper, but I want only .jpg, no .jpeg
    if ($ext == '.jpeg') $ext = '.jpg';
    if ($ext != ".gif" && $ext != ".jpg" && $ext != ".png" && $ext != ".bmp") {
        return array('errCode' => 1, 'errMessage' => $filename . ' is not an image');
    }
    $targetimg = date("YmdHis") . md5($filename) . $ext;
    $imagesrc = @file_get_contents($imgurl);
    if (!$imagesrc) {
        return array('errCode' => 2, 'errMessage' => 'Cannot get ' . $imgurl);
    }
    $imagetgt = fopen(MYBB_ROOT . $mybb->settings['autodownpics_path'] . $targetimg, 'w');
    chmod(MYBB_ROOT . $mybb->settings['autodownpics_path'] . $targetimg, 0755);
    fwrite($imagetgt, $imagesrc);
    $size = getimagesize(MYBB_ROOT . $mybb->settings['autodownpics_path'] . $targetimg);
    fclose($imagetgt);
    if (($size['mime'] == 'image/png') || ($size['mime'] == 'image/jpeg') || ($size['mime'] == 'image/gif') || ($size['mime'] == 'image/bmp')) {
        return array('errCode' => 0, 'local' => $targetimg);
    } else {
        unlink(MYBB_ROOT . $mybb->settings['autodownpics_path'] . $targetimg);
        return array('errCode' => 1, 'arrMessage' => $filename . ' is not an image');
    }
}
