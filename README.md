# ABP Auto-Download Pictures

When an user post an external picture, the picture is downloaded and the local one is displayed.

**IMPORTANT** : The plugin is a beta one, it works but ***could expose your forum***. Be really carreful when testing it.

# Settings
* Upload directory: directory where the pictures are stocked
* Authorized groups: only members of these groups have their pictures downloaded
* Active forums: the plugin works only on the choosen forums 

# TODO
Working mode: choose if the given picture must be shown as an image or as an attachment